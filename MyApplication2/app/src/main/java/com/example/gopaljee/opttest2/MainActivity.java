package com.example.gopaljee.opttest2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.matesnetwork.Cognalys.VerifyMobile;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    final static String YOUR_APP_ID = "2541938407984f5aa4ad618";
    final static String YOUR_ACCESS_TOKEN = "e4eb04706ce25164ec2a05d951053a01b96ee55d";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClick(View view) {
        EditText countryCode = (EditText)findViewById(R.id.editText2);
        EditText phoneNum = (EditText)findViewById(R.id.editText1);

        String s = countryCode.getText().toString()+phoneNum.getText().toString();
        Intent in = new Intent(MainActivity.this, VerifyMobile.class);
        in.putExtra("app_id", YOUR_APP_ID);
        in.putExtra("access_token", YOUR_ACCESS_TOKEN);
        in.putExtra("mobile", s);

        startActivityForResult(in, VerifyMobile.REQUEST_CODE);

    }

    @Override
    protected void onActivityResult(int arg0, int arg1, Intent arg2) {

        if (arg0 == VerifyMobile.REQUEST_CODE) {
            String message = arg2.getStringExtra("message");
            int result=arg2.getIntExtra("result", 0);
            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
            Toast.makeText(getApplicationContext(), result, Toast.LENGTH_SHORT).show();
        }
        super.onActivityResult(arg0, arg1, arg2);

    }
}
